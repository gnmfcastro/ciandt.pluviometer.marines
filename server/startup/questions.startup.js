Meteor.startup( function (){
  if(Questions.find().count()===0){
    //question1
    Questions.insert({question:"Tomou Banho?",type:"unique",options:["Sim","Não"],action:"banho"});
    Questions.insert({question:"Quantas vezes?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"banho"});
    Questions.insert({question:"Qual a duração média?",type:"avgTime",options:[5,10,15,20,30,35,40,45,50,55,60],action:"banho"});
    //question2
    Questions.insert({questions:"Escovou os dentes?",type:"unique",options:["Sim","Não"],action:"escovacao"});
    Questions.insert({question:"Quantas vezes?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"escovacao"});
    Questions.insert({question:"Torneira aberta ou fechada?",type:"unique",options:["Sim","Não"],action:"escovacao"});
    Questions.insert({question:"Qual a duração média?",type:"avgTime",options:[5,10,15],action:"escovacao"});
    //question3
    Questions.insert({questions:"Fez barba?",type:"unique",options:["Sim","Não"],action:"barba"});
    Questions.insert({questions:"Quantas vezes no mês?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"barba"});

    Questions.insert({questions:"Foi ao banheiro e deu descarga?",type:"unique",options:["Sim","Não"],action:"descarga"});
    Questions.insert({questions:"Quantas vezes?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"descarga"});

    Questions.insert({questions:"Lavou as mãos?",type:"unique",options:["Sim","Não"],action:"maos"});
    Questions.insert({questions:"Quantas vezes?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"maos"});

    Questions.insert({questions:"Bebeu agua?",type:"unique",options:["Sim","Não"],action:"bebeu"});
    Questions.insert({questions:"Quantos Litros?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"bebeu"});

    Questions.insert({questions:"Lavou as louças?",type:"unique",options:["Sim","Não"],action:"louca"});
    //Questions.insert({questions:"Como?",type:"multiple",options:["Pia","Máquina de lavar louça"],action:"louca"});
    Questions.insert({questions:"Quanto tempo?",type:"avgTime",options:[5,10,15,20,30,35,40,45,50,55,60],action:"louca"});
    Questions.insert({questions:"Quantas vezes?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"louca"});

    Questions.insert({questions:"Lavou carro?",type:"unique",options:["Sim","Não"],action:"carro"});
    //Questions.insert({questions:"Como?",type:"multiple",options:["Lava rápido","Balde","Mangueira"],action:"carro"});
    Questions.insert({questions:"Quanto tempo levou em média?",type:"avgTime",options:[5,10,15,20,30,35,40,45,50,55,60],action:"carro"});
    Questions.insert({questions:"Quantas vezes no mês?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"carro"});

    Questions.insert({questions:"Lavou o quintal?",type:"unique",options:["Sim","Não"],action:"quintal"});
    //Questions.insert({questions:"Como?",type:"multiple",options:["Mangueira","Balde"],action:"quintal"});
    Questions.insert({questions:"Quanto tempo levou em média?",type:"avgTime",options:[10,20,30,40,50,60],action:"quintal"});
    Questions.insert({questions:"Quantas vezes no mês?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"quintal"});


    Questions.insert({questions:"Lavou roupas em casa?",type:"unique",options:["Sim","Não"],action:"roupa"});
    //Questions.insert({questions:"Como?",type:"unique",options:["Maquina","Tanquinho","Tanque"],action:"roupa"});
    Questions.insert({questions:"Quanto tempo levou em média?",type:"avgTime",options:[10,20,30,40,50,60],action:"roupa"});
    Questions.insert({questions:"Quantas vezes no mês?",type:"quantity",options:[1,2,3,4,5,6,7,8,9,10],action:"roupa"});
  }

//  Answers.insert({action:"banho",quantity:"3",avg-time:"10"});


//legenda: type = 0 , pegar o valor fixo
//type 1 , multiplicar fator por liters
//type 2, multiplicar fator pela liters2
  if(CalcBase.find().count()===0){
      CalcBase.insert({action:"banho",type:"1",liters:"9",daily:"1"});
      //CalcBase.insert({action:"escovacao",type:"0",liters:"3",daily:"1"});
      CalcBase.insert({action:"escovacao",type:"1",liters:"2.4",daily:"1"});
      CalcBase.insert({action:"maos",type:"0",liters:"2.4",daily:"1"});
      CalcBase.insert({action:"barba",type:"1",liters:"2.4",daily:"0"});
      CalcBase.insert({action:"descarga",type:"0",liters:"14",daily:"1"});
      CalcBase.insert({action:"louca",type:"2",liters:"7.8",liters2:"0.78",daily:"1"});
      CalcBase.insert({action:"roupa",type:"1",liters:"18.6",daily:"0"});
      //CalcBase.insert({action:"roupa",type:"0",liters:"135",daily:"0"});
      CalcBase.insert({action:"quintal",type:"1",liters:"18.6",daily:"0"});
      //CalcBase.insert({action:"quintal",type:"0",liters:"60",daily:"0"});
      //CalcBase.insert({action:"carro",type:"0",liters:"80",waterSource:"balde",daily:"0"});
      //CalcBase.insert({action:"carro",type:"0",liters:"300",waterSource:"lava rapido",daily:"0"});
      CalcBase.insert({action:"carro",type:"1",liters:"18.6",waterSource:"mangueira",daily:"0"});
  }

  if(InvoiceBase.find().count()===0){
    InvoiceBase.insert({quantity:"10",fixedPrice:"20.64",});
    InvoiceBase.insert({quantity:"20",fixedPrice:"20.64",variablePrice:"2.88"});
    InvoiceBase.insert({quantity:"50",fixedPrice:"49.44",variablePrice:"3.81"});
    InvoiceBase.insert({quantity:"9999999",fixedPrice:"163.74",vari
