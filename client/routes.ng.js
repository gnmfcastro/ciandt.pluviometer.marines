angular.module('ciandt.pluviometer').run(["$rootScope", "$state","$mdToast", "$meteor", function($rootScope, $state, $mdToast, $meteor) {

  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the $meteor.requireUser() promise is rejected
    // and redirect the user back to the login page
    //if (error === "AUTH_REQUIRED") {
        // It is better to use $state instead of $location. See Issue #283.
      //  $state.go('login');
    //}
  });
}]);

angular.module('ciandt.pluviometer').config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
  function($urlRouterProvider, $stateProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $stateProvider
      .state('home', {
        url: '/',
        views: {
          "main": {
            templateUrl:'client/views/home.ng.html',
            controller:'HomeCtrl'
          }
        }
      });

      $urlRouterProvider.otherwise('/');
}]);
