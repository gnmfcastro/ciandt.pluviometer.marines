angular.module('ciandt.pluviometer').controller("HomeCtrl",function($rootScope,$scope,$meteor,$state){

  $scope.navigateTo = function(to){
	  $state.go(to);
  }


  $scope.result = function (){
    var monthlyWaterExpentValue = 0;
    var monthlyWaterExpentInMeters = 0;
    var monthlyWaterInvoiceValue=0;
    var oldQuantity =0;

    /Acumulo mensal
    answers.foreach(function (answer){
      var calcBase = CalcBase.find({action:answer.action});

      if(calcBase.type===0){
        monthlyWaterExpentValue += (calcBase.liters * answer.quantity)*((calcBase.daily==="1")?30:1);
      }
      else if(calcBase.type===1){
        monthlyWaterExpentValue += (calcBase.liters * answer.quantity * answer.avgTime)*((calcBase.daily==="1")?30:1);
      }
      else{
        monthlyWaterExpentValue += (calcBase.liters2 * answer.quantity * answer.avgTime)*((calcBase.daily==="1")?30:1);
      }

    });

    //Convertion from liters to m³
    monthlyWaterExpentInMeters = monthlyWaterExpentValue/1000;


    InvoiceBase.forEach(function(invoiceBaseItem){
      if(monthlyWaterExpentInMeters<=invoiceBaseItem.quantity){
        monthlyWaterInvoiceValue=(invoiceBaseItem.fixedPrice + (monthlyWaterExpentInMeters-oldQuantity)*invoiceBaseItem.variablePrice)*2;
      }
      oldQuantity = invoiceBaseItem.quantity;
    });

    return {monthlyWaterExpent : monthlyWaterExpentValue,monthlyWaterInvoice : monthlyWaterInvoiceValue}

  }

});
